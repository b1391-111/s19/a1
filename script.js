// Exponent
let getCube = 2 ** 3;

// Address Array Destructuring 
let address = [255, "Washington Ave NW", "California", 90011];
const [houseNumber, avenue, state, postal] = address;

// Animal object destructuring
let animal  = {
    animalName: "lolong",
    animalType: "saltwater crocodile",
    animalWeight: 1075,
    animalMeasurement: {
        feet: 20,
        inches: 3
    }
}
const {animalName, animalType, animalWeight, animalMeasurement} = animal;

// Arrow function implicit return
let numbers = [1, 2, 3, 4, 5];
let sum = numbers.reduce((a, b) => a + b);

// Dog Class
class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}
const frankie = new Dog("Frankie", 5, "Miniature Daschshund");

// Logging to the console
console.log(`the cube of 2 is ${getCube}`);
console.log(`I live at ${houseNumber} ${avenue} ${state} ${postal}`);
console.log(`${animalName} was a ${animalType}. He weighed at ${animalWeight}kgs with a measurment of ${animalMeasurement.feet}ft ${animalMeasurement.inches}in`);
numbers.forEach(num => console.log(num));
console.log(sum)
console.log(frankie);
